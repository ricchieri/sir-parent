<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.ricchieri</groupId>
    <artifactId>sir-parent</artifactId>
    <version>0.0.8-SNAPSHOT</version>
    <name>sir-parent</name>
    <description>All the things common in all the projects</description>
    <packaging>pom</packaging>

    <developers>
        <developer>
            <email>sacco.andres@gmail.com</email>
            <name>Andres Sacco</name>
            <id>sacco.andres</id>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git@gitlab.com:ricchieri/lib/sir-parent.git</connection>
        <url>https://gitlab.com/ricchieri/lib/sir-parent</url>
        <developerConnection>scm:git:git@gitlab.com:ricchieri/lib/sir-parent.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <resource.delimiter>@</resource.delimiter>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <spring-boot.version>2.2.6.RELEASE</spring-boot.version>

        <!-- Plugins -->
        <java.version>11</java.version>
        <build-helper-maven-plugin.version>3.0.0</build-helper-maven-plugin.version>
        <wiremock-maven-plugin.version>4.4.0</wiremock-maven-plugin.version>
        <docker-maven-plugin.version>0.20.1</docker-maven-plugin.version>
        <snyk-maven-plugin.version>1.2.6</snyk-maven-plugin.version>
        <jacoco.version>0.8.2</jacoco.version>
        <skipUnitTests>false</skipUnitTests>

        <maven-shade-plugin.version>3.2.2</maven-shade-plugin.version>
        <maven-surefire-plugin.version>2.22.2</maven-surefire-plugin.version>
        <maven-failsafe-plugin.version>2.22.0</maven-failsafe-plugin.version>
        <maven-javadoc-plugin.version>3.1.1</maven-javadoc-plugin.version>
        <maven-release-plugin.version>3.0.0-M1</maven-release-plugin.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <maven-compiler-plugin.version>3.8.0</maven-compiler-plugin.version>
        <maven-resources-plugin.version>3.1.0</maven-resources-plugin.version>
        <skip.surefire.tests>false</skip.surefire.tests>
        <formatter-maven-plugin.version>2.8.1</formatter-maven-plugin.version>
        <git-build-hook-maven-plugin.version>3.0.0</git-build-hook-maven-plugin.version>

        <!-- Artifactory -->
        <bintray.subject>ricchieri</bintray.subject>
        <bintray.repo>sir</bintray.repo>
        <bintray.package>sir-parent</bintray.package>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>

                <!-- Release plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven-release-plugin.version}</version>
                    <configuration>
                        <goals>clean deploy</goals>
                        <pushChanges>true</pushChanges>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                        <!--suppress UnresolvedMavenProperty -->
                        <scmReleaseCommitComment>@{prefix} [ci skip] prepare release: ${releaseVersion}</scmReleaseCommitComment>
                        <!--suppress UnresolvedMavenProperty -->
                        <scmDevelopmentCommitComment>@{prefix} prepare for next development iteration: ${developmentVersion}.</scmDevelopmentCommitComment>
                    </configuration>
                </plugin>

                <!-- Add source code to jar -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <goals>
                                <goal>jar-no-fork</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <!-- Package all code together with shade -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>${maven-shade-plugin.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.springframework.boot</groupId>
                            <artifactId>spring-boot-maven-plugin</artifactId>
                            <version>${spring-boot.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <keepDependenciesWithProvidedScope>true</keepDependenciesWithProvidedScope>
                        <createDependencyReducedPom>false</createDependencyReducedPom>
                        <filters>
                            <filter>
                                <artifact>*:*</artifact>
                                <excludes>
                                    <exclude>META-INF/*.SF</exclude>
                                    <exclude>META-INF/*.DSA</exclude>
                                    <exclude>META-INF/*.RSA</exclude>
                                </excludes>
                            </filter>
                        </filters>
                    </configuration>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>shade</goal>
                            </goals>
                            <configuration>
                                <transformers>
                                    <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/spring.handlers</resource>
                                    </transformer>
                                    <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/spring.binders</resource>
                                    </transformer>
                                    <transformer implementation="org.springframework.boot.maven.PropertiesMergingResourceTransformer">
                                        <resource>META-INF/spring.factories</resource>
                                    </transformer>
                                    <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/spring.schemas</resource>
                                    </transformer>
                                    <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
                                    <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                        <mainClass>${application.mainClass}</mainClass>
                                    </transformer>
                                </transformers>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <!-- Security -->
            <plugin>
                <groupId>io.snyk</groupId>
                <artifactId>snyk-maven-plugin</artifactId>
                <version>${snyk-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>snyk-test</id>
                        <goals>
                            <goal>test</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>snyk-monitor</id>
                        <goals>
                            <goal>monitor</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <apiToken>${SNYK_API_TOKEN}</apiToken>
                    <failOnSeverity>higher</failOnSeverity>
                    <org />
                </configuration>
            </plugin>

            <!-- Compiler Plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>

            <!-- Compile without consider javadoc correctness, important in java 1.8 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven-javadoc-plugin.version}</version>
                <configuration>
                    <additionalOptions>
                        <additionalOption>-Xdoclint:none</additionalOption>
                    </additionalOptions>
                </configuration>
            </plugin>

            <!-- Testing pluggins -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <skipTests>${skip.surefire.tests}</skipTests>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${maven-failsafe-plugin.version}</version>
            </plugin>

            <!-- Add conf to jar -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/config</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Filter resources plugin config -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven-resources-plugin.version}</version>
                <configuration>
                    <delimiters>
                        <delimiter>${resource.delimiter}</delimiter>
                    </delimiters>
                </configuration>
            </plugin>

            <!-- Code formatter -->
            <plugin>
                <groupId>net.revelc.code.formatter</groupId>
                <artifactId>formatter-maven-plugin</artifactId>
                <version>${formatter-maven-plugin.version}</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>validate</goal>
                            <goal>format</goal>
                        </goals>
                        <configuration>
                            <includes>
                                <include>**/*.java</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Hooks of git -->
            <plugin>
                <groupId>com.rudikershaw.gitbuildhook</groupId>
                <artifactId>git-build-hook-maven-plugin</artifactId>
                <version>${git-build-hook-maven-plugin.version}</version>
                <configuration>
                    <gitConfig>
                        <!-- The location of the directory you are using to store the Git hooks in your project. -->
                        <core.hooksPath>hooks/</core.hooksPath>
                        <!-- Some other project specific git config that you want to set. -->
                        <custom.configuration>true</custom.configuration>
                    </gitConfig>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <!-- Initialize a Git repository at the root of the project if one does not exist. -->
                            <goal>initialize</goal>
                            <goal>install</goal>
                            <goal>configure</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <distributionManagement>
        <repository>
            <id>bintray-ricchieri-sir</id>
            <name>ricchieri-sir</name>
            <url>https://api.bintray.com/maven/${bintray.subject}/${bintray.repo}/${bintray.package}/;publish=1</url>
        </repository>
    </distributionManagement>
</project>
